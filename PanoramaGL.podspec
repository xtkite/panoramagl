#
# Be sure to run `pod lib lint PanoramaGL.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "PanoramaGL"
  s.version          = "0.1.0"
  s.summary          = "A short description of PanoramaGL."
  s.description      = <<-DESC
                       An optional longer description of PanoramaGL

                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "https://github.com/<GITHUB_USERNAME>/PanoramaGL"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'Apache'
  s.author           = { "jb" => "javbaezga@gmail.com" }
  s.source           = { :git => "https://github.com/<GITHUB_USERNAME>/PanoramaGL.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = false

  s.source_files = 'PanoramaGL/**/*.{h,m,c}'
  #s.resource_bundles = {
  #  'PanoramaGL' => ['Pod/Assets/*.png']
  #}

  # s.public_header_files = 'PanoramaGL/**/*.h'
  s.frameworks = 'CoreMotion', 'CoreLocation', 'MobileCoreServices', 'SystemConfiguration', 'QuartzCore', 'OpenGLES', 'CFNetwork', 'UIKit', 'Foundation', 'CoreGraphics'
  # s.dependency 'AFNetworking', '~> 2.3'
end
