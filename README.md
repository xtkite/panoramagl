# PanoramaGL

[![CI Status](http://img.shields.io/travis/ab/PanoramaGL.svg?style=flat)](https://travis-ci.org/ab/PanoramaGL)
[![Version](https://img.shields.io/cocoapods/v/PanoramaGL.svg?style=flat)](http://cocoapods.org/pods/PanoramaGL)
[![License](https://img.shields.io/cocoapods/l/PanoramaGL.svg?style=flat)](http://cocoapods.org/pods/PanoramaGL)
[![Platform](https://img.shields.io/cocoapods/p/PanoramaGL.svg?style=flat)](http://cocoapods.org/pods/PanoramaGL)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PanoramaGL is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PanoramaGL"
```

## License

PanoramaGL is available under the Apache License. See the LICENSE file for more info.
